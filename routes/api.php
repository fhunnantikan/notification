<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
  return $request->user();
});

Route::get('/test', function () {
return response()->json([
  'data' => 'Test api response'
]);
});



Route::post('/notify', function(\Request $request) {
$input = $request::all();
$title = \Request::input('title');
$message = \Request::input('message');

$content = array(
  'en' => $message,
        );
$headings = array(
  'en' => $title
);

$fields = array(
  'app_id' => "b12c5be8-a04a-4d58-91ce-8918f3446805",
  'included_segments' => array('All'),
  'data' => array(
      "foo" => "bar"
  ),
  'contents' => $content,
  'headings' => $headings,
  'url' => 'https://onesignal.com',
);

$fields = json_encode($fields);
print("\nJSON sent:\n");
print($fields);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json; charset=utf-8',
    'Authorization: Basic NDhlODc4MmEtOGNkZC00NTBlLThiYWMtYmVmMjI0NzJjZDVj'
));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

$response = curl_exec($ch);
curl_close($ch);

return $response;
});

Route::post('/noti', function(\Request $request) {
  $input = $request::all();
  $title_second = \Request::input('title_second');
  $message_second = \Request::input('message_second');
  $user_name = \Request::input('user_name');
  //return $name;
  
  $content_second = array(
    'en' => $message_second,
          );
  $headings_second = array(
    'en' => $title_second
  );
  
  $fields = array(
    'app_id' => "b12c5be8-a04a-4d58-91ce-8918f3446805",
    'filters' => array(array("field" => "tag", "key" => "Username", "relation" => "=", "value" => $user_name)),
    'data' => array("foo" => "bar"),
    'contents' => $content_second,
    'headings' => $headings_second,
  );
  
  $fields = json_encode($fields);
    print("\nJSON sent:\n");
    print($fields);
  
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic NDhlODc4MmEtOGNkZC00NTBlLThiYWMtYmVmMjI0NzJjZDVj'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    return $response;
   
    });