const routes = [
  {
    path: '/',
   component: () => import('layouts/MyLayout.vue'),
   children: [
     { path: '', component: () => import('pages/Index.vue') }
    ]
 }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '/h',
    component: () => import('pages/notification.vue')
  })
}
export default routes